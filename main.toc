\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Intermodaler Verkehr: Risiken und zuk\"unftige Bedeutung}{2}{subsection.1.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}Das Forschungsprojekt FENIX}{3}{subsection.1.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Zielsetzung}{4}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Methodik der Arbeit}{4}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Grundlagen und Stand der Technik}{7}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Grundlagen der Logistik}{7}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}G\"uterverkehr und Transportketten}{9}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}Eingliedrige Transportketten}{10}{subsection.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}Mehrgliedrige Transportketten}{10}{subsection.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Intermodaler G\"utertransport}{12}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Verwandte St\"orungen im Transportablauf}{13}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Softwaredesign}{14}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Informationstechnologien des Web}{14}{subsection.2.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}Grundlagen des Requirements Engineering}{15}{subsection.2.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.3}Modellierung in der Softwareentwicklung}{18}{subsection.2.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Anforderungen}{21}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Methodik zur Anforderungsanalyse}{21}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Experteninterviews}{22}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Stakeholder Identifikation}{22}{subsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Konzept und Ablauf der Interviews}{22}{subsection.3.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Fragenkatalog}{22}{subsection.3.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Ergebnisse}{22}{subsection.3.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Workshops}{23}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Zielsetzung und Rollen im Workshop}{23}{subsection.3.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Konzept und Ablauf des Workshops}{24}{subsection.3.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Ergebnisse}{29}{subsection.3.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Zwischenfazit}{34}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Zwischenfazit zu Anforderungen}{34}{subsection.3.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Ableitung von Anwendungsf\"allen}{34}{subsection.3.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Erarbeitung eines St\"orungsmanagements}{37}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Anwendungsfälle}{38}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Anwendungsfall: Fahrerausfall}{38}{subsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Anwendungsfall: Fahrzeugausfall}{39}{subsection.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Anwendungsfall: Linienausfall}{39}{subsection.4.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Störungsmanagement und Architektur}{40}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Modell des Störungsmanagements}{40}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}System Architektur}{45}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2.1}Methodenkatalog}{46}{subsubsection.4.2.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2.2}Störungsobjekt}{47}{subsubsection.4.2.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Aufbau Störungskatalog}{50}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Implementierung}{51}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Design User Interface}{52}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Oberfl\"achenkonzept}{52}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Komponenten der Webapplikation}{52}{subsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Simulation}{53}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Simulation von Stammdaten}{53}{subsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Simulation von Anwendungsf\"allen}{53}{subsection.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Evaluation}{55}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Auswertung}{55}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Fazit}{55}{section.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Zusammenfassung und Ausblick}{57}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Anhang}{i}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Schriftliche Ausarbeitung}{i}{section.A.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}Vorgehensweise}{iii}{section.A.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}Editoren}{iv}{section.A.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{v}{chapter*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{vii}{chapter*.35}%
